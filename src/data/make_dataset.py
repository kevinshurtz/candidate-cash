import requests
import json
import time
import pickle
import os
import dotenv
import election_context

def main():
    ''' Gather data on all House and Senate candidates and their victories (four datasets
        total), serialize the data using pickle, and store it in the "interim" directory
        of the "data" directory for further processing
    '''
    # Get sensitive information from the environment variables saved in .env
    dotenv_path = dotenv.find_dotenv()
    dotenv.load_dotenv(dotenv_path)

    fec_api_key = os.environ.get('FEC_API_KEY')
    propublica_api_key = os.environ.get('PROPUBLICA_API_KEY')

    interim_data_path = '../../data/interim/'

    # Generate and save a dictionary of cycles for which candidates were successful
    start_year = 2016
    end_year = 2020

    senate_candidate_cycles = get_senate_candidate_cycles(start_year, end_year, fec_api_key, propublica_api_key)
    house_candidate_cycles = get_house_candidate_cycles(start_year, end_year, fec_api_key, propublica_api_key)

    senate_candidate_cycles_file_path = interim_data_path + 'senate_candidate_cycles.pkl'
    house_candidate_cycles_file_path = interim_data_path + 'house_candidate_cycles.pkl'

    with open(senate_candidate_cycles_file_path, 'wb') as file:
        pickle.dump(senate_candidate_cycles, file)

    with open(house_candidate_cycles_file_path, 'wb') as file:
        pickle.dump(house_candidate_cycles, file)

    # Generate and save the object representation of all Senate and House candidates
    senate_cycles = [year for year in range(start_year, end_year, 2)]
    house_cycles = [year for year in range(start_year, end_year, 2)]

    senate_candidates = list()
    house_candidates = list()

    # Combine all senators
    for cycle in senate_cycles:
        for state in election_context.State:
            candidates = get_senate_candidates(cycle, state, fec_api_key)
            senate_candidates.extend(candidates)
    
    # Combine all members of the House
    for cycle in house_cycles:
        for state in election_context.State:
            # Use the number of districts unique to each state
            house_num_districts = election_context.get_house_num_districts()

            for district in range(house_num_districts[state]):
                candidates = get_house_candidates(cycle, state, district, fec_api_key)
                house_candidates.extend(candidates)
    
    senate_candidates_file_path = interim_data_path + 'senate_candidates.pkl'
    house_candidates_file_path = interim_data_path + 'house_candidates.pkl'

    with open(senate_candidates_file_path, 'wb') as file:
        pickle.dump(senate_candidates, file)

    with open(house_candidates_file_path, 'wb') as file:
        pickle.dump(house_candidates, file)

def get_fec_data_file_path(path, params, headers, page, pages):
    ''' Get the full path and file name of a stored JSON file given the
        URI path, URI parameters, headers, page, and number of pages
        associated with a set of web requests to the FEC API
    '''
    raw_data_path = '../../data/raw/'

    office = params['office']
    cycle = params['cycle']

    if office == 'senate':
        state = params['state']
        file_name = f'{office}_{state}_{cycle}_{page}_of_{pages}.json'

    if office == 'house':
        state = params['state']
        district = params['district']
        file_name = f'{office}_{state}_{district}_{cycle}_{page}_of_{pages}.json'
    
    return raw_data_path + file_name

def get_propublica_data_file_path(path, params, headers):
    ''' Get the full path and file name of a stored JSON file given the
        URI path, URI parameters, and headers, associated with a set of
        web requests to the ProPublica API
    '''
    raw_data_path = '../../data/raw/'
    path_data = str.split(path, '/')

    term = path_data[0]
    office = path_data[1]

    file_name = f'{term}_{office}_members.json'
    return raw_data_path + file_name

def save_fec_data(path, params, headers, content):
    ''' Save FEC data in JSON format
    '''
    page = content['pagination']['page']
    pages = content['pagination']['page']

    json_file_path = get_fec_data_file_path(path, params, headers, page, pages)

    with open(json_file_path, 'w+') as file:
        json.dump(content, file)

def save_propublica_data(path, params, headers, content):
    ''' Save ProPublica data in JSON format
    '''
    json_file_path = get_propublica_data_file_path(path, params, headers)

    with open(json_file_path, 'w+') as file:
        json.dump(content, file)

def get_fec_data(path, params, headers):
    ''' Get data from the openFEC API
    '''
    uri = f'https://api.open.fec.gov/v1/{path}'

    '''
    While it is not technically possible to know the total number of pages that can be
    returned in the result set of an API call, it is typical that the the result set will
    only require a single page, and that this will be reflected in the filename of the cached
    JSON file.  We can start by assuming this is the case.
    
    If this is not the case, then we will make a request for the first page, thereby 
    determining how many pages remain and the naming scheme of any remaining pages.
    '''

    # Assume the result set only requires a single page
    json_file_path = get_fec_data_file_path(path, params, headers, 1, 1)

    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as file:
            content = json.load(file)
    else:
        # Make a new request for the data
        response = requests.get(uri, params=params)

        if not response.ok:
            response.raise_for_status()

        # Load and cache the data
        content = json.loads(response.content)
        save_fec_data(path, params, headers, content)

    results = content['results']

    # Append any additonal pages to the result set
    page = content['pagination']['page']
    pages = content['pagination']['pages']

    while page < pages:
        next_params = params
        next_params['page'] = page

        # Check if the data has been cached
        json_file_path = get_fec_data_file_path(path, next_params, headers, page, pages)

        if os.path.exists(json_file_path):
            with open(json_file_path, 'r') as file:
                next_content = json.load(file)
        else:
            # If no data can be found, make another web request
            next_response = requests.get(uri, params=next_params, headers=headers)

            if not next_response.ok:
                response.raise_for_status()

            # Load and cache the data
            next_content = json.loads(next_response.content)
            save_fec_data(path, params, headers, next_content)

        results += next_content['results']
        page += 1
    
    return results

def get_propublica_data(path, params, headers):
    ''' Get data from the ProPublica API
    '''
    # Load JSON content from disk or make a new request
    json_file_path = get_propublica_data_file_path(path, params, headers)

    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as file:
            content = json.load(file)
    else:
        uri = f'https://api.propublica.org/congress/v1/{path}'
        response = requests.get(uri, params=params, headers=headers)

        if not response.ok:
            response.raise_for_status()

        content = json.loads(response.content)

    return content['results']

def get_president_candidates(cycle, fec_key):
    ''' Get candidates from a given cycle running for the office of President
    '''
    uri_path = '/elections/'
    query_params = {
        'cycle': cycle,
        'office': election_context.Office.PRESIDENT.name.lower(),
        'per_page': 100,
        'api_key': fec_key
    }
    headers = dict()

    return get_fec_data(uri_path, query_params, headers)

def get_senate_candidates(cycle, state, fec_key):
    ''' Get candidates from a given cycle running for the office of Senate
    '''
    uri_path = '/elections/'
    query_params = {
        'cycle': cycle,
        'office': election_context.Office.SENATE.name.lower(),
        'state': state.name.upper(),
        'per_page': 100,
        'api_key': fec_key
    }
    headers = dict()

    return get_fec_data(uri_path, query_params, headers)

def get_house_candidates(cycle, state, district, fec_key):
    ''' Get candidates from a given cycle running for the office of President
    '''
    uri_path = '/elections/'
    query_params = {
        'cycle': cycle,
        'office': election_context.Office.HOUSE.name.lower(),
        'state': state.name.upper(),
        'district': f'{district:02}',
        'per_page': 100,
        'api_key': fec_key
    }
    headers = dict()

    return get_fec_data(uri_path, query_params, headers)

def get_senate_members(term, propublica_key):
    ''' Get all members of the House from a given term of Congress (earliest term is 80)
    '''
    uri_path = f'{term}/senate/members.json'
    query_params = dict()
    headers = {
        'X-API-KEY': propublica_key
    }
    
    data = get_propublica_data(uri_path, query_params, headers)
    return data[0]['members']

def get_house_members(term, propublica_key):
    ''' Get all members of the House from a given term of Congress (earliest term is 102)
    '''
    uri_path = f'{term}/house/members.json'
    query_params = dict()
    headers = {
        'X-API-KEY': propublica_key
    }
    
    data = get_propublica_data(uri_path, query_params, headers)
    return data[0]['members']

def get_cycle_term(cycle):
    ''' Given an election cycle, get the corresponding term of Congress
    '''
    # The first congress was held in 1789, and a term lasts 2 years
    first_term_year = 1789
    term_length = 2

    return int((cycle - (first_term_year - 1)) / term_length)

def get_senate_candidate_cycles(start_year, end_year, fec_api_key, propublica_api_key):
    ''' Get a dictionary of senate candidate IDs mapped to a set of election cycles in which they won
    '''
    senate_cycles = [year for year in range(start_year, end_year, 2)]
    senate_candidate_cycles = dict()

    for cycle in senate_cycles:
        # Get all senator candidate FEC IDs
        for state in election_context.State:
            senate_cands = get_senate_candidates(cycle, state, fec_api_key)

            # Initialize each candidate to an empty set
            for senate_cand in senate_cands:
                senate_candidate_cycles[senate_cand['candidate_id']] = set()
        
        # Get all senate members for the following term
        term = get_cycle_term(cycle)
        senate_members = get_senate_members(term, propublica_api_key)

        # Add each victorious cycle to the set mapped to each candidate ID
        for senator in senate_members:
            fec_id = senator['fec_candidate_id']

            # Ensure that candidate ID is in the set of candidates
            if fec_id in senate_candidate_cycles.keys():
                senate_candidate_cycles[fec_id].add(cycle)
    
    return senate_candidate_cycles

def get_house_candidate_cycles(start_year, end_year, fec_api_key, propublica_api_key):
    ''' Get a dictionary of house candidate IDs mapped to a set of election cycles in which they won
    '''
    house_cycles = [year for year in range(start_year, end_year, 2)]
    house_candidate_cycles = dict()

    # Get all house candidate FEC IDs
    for cycle in house_cycles:
        for state in election_context.State:
            # Use the number of districts unique to each state
            house_num_districts = election_context.get_house_num_districts()

            for district in range(house_num_districts[state]):
                house_cands = get_house_candidates(cycle, state, district, fec_api_key)

                # Initialize each candidate to an empty set
                for house_cand in house_cands:
                    house_candidate_cycles[house_cand['candidate_id']] = set()
        
        # Get all house members for the following term
        term = get_cycle_term(cycle)
        house_members = get_house_members(term, propublica_api_key)

        # Add each victorious cycle to the set mapped to each candidate ID
        for house_member in house_members:
            fec_id = house_member['fec_candidate_id']

            # Ensure that candidate ID is in the set of candidates
            if fec_id in house_candidate_cycles.keys():
                house_candidate_cycles[fec_id].add(cycle)
    
    return house_candidate_cycles

if __name__ == '__main__':
    main()

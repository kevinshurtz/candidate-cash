import enum

class Office(enum.Enum):
    ''' An enum for each of the three types of offices for which a candidate
        can run in an election
    '''
    HOUSE = 1
    SENATE = 2
    PRESIDENT = 3

class State(enum.Enum):
    ''' An enum for each of the states in which a candidate can run for a House
        or Senate seat
    '''
    AL = 1
    AK = 2
    AZ = 3
    AR = 4
    CA = 5
    CO = 6
    CT = 7
    DE = 8
    DC = 9
    FL = 10
    GA = 11
    HI = 12
    ID = 13
    IL = 14
    IN = 15
    IA = 16
    KS = 17
    KY = 18
    LA = 19
    ME = 20
    MT = 21
    NE = 22
    NV = 23
    NH = 24
    NJ = 25
    NM = 26
    NY = 27
    NC = 28
    ND = 29
    OH = 30
    OK = 31
    OR = 32
    MD = 33
    MA = 34
    MI = 35
    MN = 36
    MS = 37
    MO = 38
    PA = 39
    RI = 40
    SC = 41
    SD = 42
    TN = 43
    TX = 44
    UT = 45
    VT = 46
    VA = 47
    WA = 48
    WV = 49
    WI = 50
    WY = 51

# A dictionary mapping States to their number of districts
def get_house_num_districts():
    return {
        State.AL: 7,
        State.AK: 0,
        State.AZ: 9,
        State.AR: 4,
        State.CA: 53,
        State.CO: 7,
        State.CT: 5,
        State.DE: 0,
        State.DC: 0,
        State.FL: 27,
        State.GA: 14,
        State.HI: 2,
        State.ID: 2,
        State.IL: 18,
        State.IN: 9,
        State.IA: 4,
        State.KS: 4,
        State.KY: 6,
        State.LA: 6,
        State.ME: 2,
        State.MT: 0,
        State.NE: 3,
        State.NV: 4,
        State.NH: 2,
        State.NJ: 12,
        State.NM: 3,
        State.NY: 27,
        State.NC: 13,
        State.ND: 0,
        State.OH: 16,
        State.OK: 5,
        State.OR: 5,
        State.MD: 8,
        State.MA: 9,
        State.MI: 14,
        State.MN: 8,
        State.MS: 4,
        State.MO: 8,
        State.PA: 18,
        State.RI: 2,
        State.SC: 7,
        State.SD: 0,
        State.TN: 9,
        State.TX: 36,
        State.UT: 4,
        State.VT: 0,
        State.VA: 11,
        State.WA: 10,
        State.WV: 3,
        State.WI: 8,
        State.WY: 0
    }

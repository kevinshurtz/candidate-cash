import pickle
import json

def main():
    interim_data_path = '../../data/interim/'
    processed_data_path = '../../data/processed/'

    # Retrieve a dictionary of all cycles in which various candidate IDs were victorious
    senate_candidate_cycles_file_path = interim_data_path + 'senate_candidate_cycles.pkl'
    house_candidate_cycles_file_path = interim_data_path + 'house_candidate_cycles.pkl'

    with open(senate_candidate_cycles_file_path, 'rb') as file:
        senate_candidate_cycles = pickle.load(file)

    with open(house_candidate_cycles_file_path, 'rb') as file:
        house_candidate_cycles = pickle.load(file)

    # Retrieve a list of all candidates
    senate_candidates_file_path = interim_data_path + 'senate_candidates.pkl'
    house_candidates_file_path = interim_data_path + 'house_candidates.pkl'

    with open(senate_candidates_file_path, 'rb') as file:
        senate_candidates = pickle.load(file)

    with open(house_candidates_file_path, 'rb') as file:
        house_candidates = pickle.load(file)
    
    # Create the final list of features and target data for the model
    senate_features = list()
    senate_target_propublica = list()
    senate_target_fec = list()
    senate_full = list()
    
    for candidate in senate_candidates:
        cycle = candidate['candidate_election_year']
        candidate_id = candidate['candidate_id']
        candidate_name = candidate['candidate_name']
        remaining_cash = candidate['cash_on_hand_end_period']
        number_of_committees = len(candidate['committee_ids'])
        total_disbursements = candidate['total_disbursements']
        incumbent_or_challenger = candidate['incumbent_challenge_full']
        party = candidate['party_full']

        other = not party == 'REPUBLICAN PARTY'
        other = other and not party == 'DEMOCRACTIC PARTY'
        other = other and not party == 'LIBERTARIAN PARTY'
        other = other and not party == 'GREEN PARTY'

        senate_features.append({
            'election_year': int(cycle),
            'remaining_cash': float(remaining_cash),
            'number_of_committees': number_of_committees,
            'total_disbursements': float(total_disbursements),
            'incumbent': 1 if incumbent_or_challenger == 'Incumbent' else 0,
            'republican': 1 if party == 'REPUBLICAN PARTY' else 0,
            'democrat': 1 if party == 'DEMOCRATIC PARTY' else 0,
            'libertarian': 1 if party == 'LIBERTARIAN PARTY' else 0,
            'green': 1 if party == 'GREEN PARTY' else 0,
            'other': 1 if other else 0
        })

        senate_target_propublica.append({
            'won': 1 if cycle in senate_candidate_cycles[candidate_id] else 0
        })

        senate_target_fec.append({
            'won': candidate['won']
        })

        senate_full.append({
            'election_year': int(cycle),
            'candidate_id': candidate_id,
            'candidate_name': candidate_name,
            'remaining_cash': float(remaining_cash),
            'number_of_committees': number_of_committees,
            'total_disbursements': float(total_disbursements),
            'incumbent': 1 if incumbent_or_challenger == 'Incumbent' else 0,
            'republican': 1 if party == 'REPUBLICAN PARTY' else 0,
            'democrat': 1 if party == 'DEMOCRATIC PARTY' else 0,
            'libertarian': 1 if party == 'LIBERTARIAN PARTY' else 0,
            'green': 1 if party == 'GREEN PARTY' else 0,
            'other': 1 if other else 0,
            'won': 1 if cycle in senate_candidate_cycles[candidate_id] else 0
        })

    house_features = list()
    house_target_propublica = list()
    house_target_fec = list()
    house_full = list()

    for candidate in house_candidates:
        cycle = candidate['candidate_election_year']
        candidate_id = candidate['candidate_id']
        candidate_name = candidate['candidate_name']
        remaining_cash = candidate['cash_on_hand_end_period']
        number_of_committees = len(candidate['committee_ids'])
        total_disbursements = candidate['total_disbursements']
        incumbent_or_challenger = candidate['incumbent_challenge_full']
        party = candidate['party_full']

        other = not party == 'REPUBLICAN PARTY'
        other = other and not party == 'DEMOCRACTIC PARTY'
        other = other and not party == 'LIBERTARIAN PARTY'
        other = other and not party == 'GREEN PARTY'

        house_features.append({
            'election_year': int(cycle),
            'remaining_cash': float(remaining_cash),
            'number_of_committees': number_of_committees,
            'total_disbursements': float(total_disbursements),
            'incumbent': 1 if incumbent_or_challenger == 'Incumbent' else 0,
            'republican': 1 if party == 'REPUBLICAN PARTY' else 0,
            'democrat': 1 if party == 'DEMOCRATIC PARTY' else 0,
            'libertarian': 1 if party == 'LIBERTARIAN PARTY' else 0,
            'green': 1 if party == 'GREEN PARTY' else 0,
            'other': 1 if other else 0
        })

        house_target_propublica.append({
            'won': 1 if cycle in house_candidate_cycles[candidate_id] else 0
        })

        house_target_fec.append({
            'won': candidate['won']
        })

        house_full.append({
            'election_year': int(cycle),
            'candidate_id': candidate_id,
            'candidate_name': candidate_name,
            'remaining_cash': float(remaining_cash),
            'number_of_committees': number_of_committees,
            'total_disbursements': float(total_disbursements),
            'incumbent': 1 if incumbent_or_challenger == 'Incumbent' else 0,
            'republican': 1 if party == 'REPUBLICAN PARTY' else 0,
            'democrat': 1 if party == 'DEMOCRATIC PARTY' else 0,
            'libertarian': 1 if party == 'LIBERTARIAN PARTY' else 0,
            'green': 1 if party == 'GREEN PARTY' else 0,
            'other': 1 if other else 0,
            'won': 1 if cycle in house_candidate_cycles[candidate_id] else 0
        })
    
    # A combined set of the Senate and House races features and target data
    combined_features = senate_features + house_features
    combined_target_propublica = senate_target_propublica + house_target_propublica
    combined_target_fec = senate_target_fec + house_target_fec
    combined_full = senate_full + house_full

    # Save features to processed files
    senate_features_path = processed_data_path + 'senate_features.json'
    house_features_path = processed_data_path + 'house_features.json'
    combined_features_path = processed_data_path + 'combined_features.json'

    with open(senate_features_path, 'w+') as file:
        json.dump(senate_features, file)

    with open(house_features_path, 'w+') as file:
        json.dump(house_features, file)

    with open(combined_features_path, 'w+') as file:
        json.dump(combined_features, file)

    # Save target data to processed files
    senate_target_propublica_path = processed_data_path + 'senate_target_propublica.json'
    house_target_propublica_path = processed_data_path + 'house_target_propublica.json'
    combined_target_propublica_path = processed_data_path + 'combined_target_propublica.json'

    senate_target_fec_path = processed_data_path + 'senate_target_fec.json'
    house_target_fec_path = processed_data_path + 'house_target_fec.json'
    combined_target_fec_path = processed_data_path + 'combined_target_fec.json'

    # Save target data generated using the ProPublica API
    with open(senate_target_propublica_path, 'w+') as file:
        json.dump(senate_target_propublica, file)

    with open(house_target_propublica_path, 'w+') as file:
        json.dump(house_target_propublica, file)

    with open(combined_target_propublica_path, 'w+') as file:
        json.dump(combined_target_propublica, file)

    # Save target data generated using the FEC API
    with open(senate_target_fec_path, 'w+') as file:
        json.dump(senate_target_fec, file)

    with open(house_target_fec_path, 'w+') as file:
        json.dump(house_target_fec, file)

    with open(combined_target_fec_path, 'w+') as file:
        json.dump(combined_target_fec, file)

    # Save full data to processed files
    senate_full_path = processed_data_path + 'senate_full.json'
    house_full_path = processed_data_path + 'house_full.json'
    combined_full_path = processed_data_path + 'combined_full.json'

    with open(senate_full_path, 'w+') as file:
        json.dump(senate_full, file)

    with open(house_full_path, 'w+') as file:
        json.dump(house_full, file)

    with open(combined_full_path, 'w+') as file:
        json.dump(combined_full, file)

if __name__ == '__main__':
    main()
